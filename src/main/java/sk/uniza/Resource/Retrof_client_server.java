package sk.uniza.Resource;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import sk.uniza.Api.Device;
import sk.uniza.openweathermap.OpenWeatherOBJ;

public interface Retrof_client_server {
    @GET("1")
    Call<Device> initDevice(@Header("Authorization") String head);

    @POST("weather")
    Call<OpenWeatherOBJ> sendData(@Body OpenWeatherOBJ weatherOBJ, @Header("Authorization") String head);
}
