package sk.uniza.Resource;

import retrofit2.Call;
import retrofit2.http.POST;
import retrofit2.http.QueryMap;
import sk.uniza.openweathermap.OpenWeatherOBJ;

import java.util.Map;

public interface Retrof_client_weather {
    @POST("weather")
    Call<OpenWeatherOBJ> get_DataFromSourceServer(@QueryMap Map<String, String> param);
}
